function createNewUser() {
	let newUser = {
		firstName: prompt("Input your first name, please!"),
		lastName: prompt("Input your last name, please!"),
		
		getLogin() {
			let login = (this.firstName.substr(0, 1) + this.lastName).toLowerCase();
			return login;
		},
		getFirstName() {
			Object.defineProperty(user1, "firstName", { writable: false });
			return this.firstName;
		},
		setFirstName(newFirstName) {
			Object.defineProperty(this, "firstName", { value: newFirstName });
		},
		getLastName() {
			Object.defineProperty(user1, "lastName", { writable: false });
			return this.lastName;
		},
		setLastName(newLastName) {
			Object.defineProperty(this, "lastName", { value: newLastName });
		},
	};
    Object.defineProperty(newUser, "firstName", {
        configurable: true,
        writable: false,
    });
    Object.defineProperty(newUser, "lastName", {
        configurable: true,
        writable: false,
    });
	return newUser;
}

let user1 = createNewUser();
console.log(user1.getFirstName() + " " + user1.getLastName());
console.log(user1.getLogin());
user1.setFirstName("Alisa");
user1.setLastName("Loe");
console.log(user1.getFirstName() + " " + user1.getLastName());
console.log(user1.getLogin());