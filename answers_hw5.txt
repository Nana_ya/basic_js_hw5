1. Опишіть своїми словами, що таке метод об'єкту.
Метод об'єкту також є властивістю, що містить функцію. 
Отже, метод об'єкту - це функції, збережені як властивості об'єкту.

2. Який тип даних може мати значення властивості об'єкта?
Будь-який (number, string, boolean, undefined, object, symbol, null).

3. Об'єкт це посилальний тип даних. Що означає це поняття?
Наведу приклад. Фрагмент коду:
let user1 = {
    name: "John",
    age: 18,
}
let user2 = user1;
Object.defineProperty(user2, "age", 19);
console.log(user1.age);

Змінна user1 містить посилання на комірку пам'яті, в якій зберігається об'єкт.
Змінна user2 містить точно таке ж посилання на комірку пам'яті, в якій зберігається
той самий об'єкт. Заміняємо властивість у змінній user2, заміниться і властивість у
змінній user1. В консоль виведеться 19.